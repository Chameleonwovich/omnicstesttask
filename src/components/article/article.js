import React from "react";
import '../../styles/article.scss'
export const Article = ({article}) => {

    const date = new Date(article.publishedAt)

    const dateString = date.getDate()  + "-" +
        (date.getMonth()+1) + "-" +
        date.getFullYear() + " " +
        (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ":" +
        date.getMinutes()
    return (
        <div className='article'>
            <h2>{article.title}</h2>
            <br/>
            <img src={article.urlToImage}/>
            {article.content && <p className='content'>{article.content.substr(0,article.content.indexOf("["))+' '}<a href={article.url}>{'Read more'}</a></p>}
            <br/>
            <div className='info'>
                <p>Source: {article.source.name}</p>
                {article.author && <p>Author: {article.author}</p>}
                <p>Published at: {dateString+" UTC"}</p>
            </div>

        </div>
    )
}