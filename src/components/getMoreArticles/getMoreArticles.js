import React, {useState} from "react";
import {getNews} from "../../api";
import '../../styles/getMoreArticles.scss'

export const GetMoreArticles = ({pageId, setPageId, articles, updateArticles}) => {
    const getNewArticles = async () => {
        await getNews(pageId).then(r=>{
            updateArticles(r.articles.concat(articles))
        })
    }

    return (
        <div>
            <button onClick={()=>{
                setPageId(pageId+1)
                getNewArticles()
            }}>Get more articles</button>
        </div>
    )
}