import {Article} from "./article/article";
import {GetMoreArticles} from "./getMoreArticles/getMoreArticles";

export {Article, GetMoreArticles}