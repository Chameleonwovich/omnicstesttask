import React, {useEffect, useState} from "react";
import {getNews} from "../../api";
import {Article, GetMoreArticles} from "../../components";
import '../../styles/home.scss'

export const Home = () => {
    const [articles, setArticles] = useState([])
    const [page, setPage] = useState(2)
    useEffect(()=>{
        getData()
    }, [])

    const getData = async () => {
        await getNews(1).then(r=>{
            setArticles(r.articles)
        })
    }

    const mapArticles = (inputArticles) => {
        return inputArticles && inputArticles.sort((a,b)=>{
            return a.publishedAt < b.publishedAt ? 1:-1
        }).map((x, i) => <Article key={i} article={x}/>)
    }

    return (
        <div>
            <div className="homePage">
                <h1>Volodymyr Mykytka - Test Task</h1>
                {mapArticles(articles)}
            </div>
            <GetMoreArticles pageId={page} setPageId={setPage} articles={articles} updateArticles={setArticles}/>
        </div>
    )
}