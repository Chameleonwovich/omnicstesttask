import {API_KEY} from "../../consts";

const axios = require("axios");

const url = 'https://newsapi.org/v2/top-headlines?' +
    'country=us&' +
    'pageSize=10&' +
    'apiKey='+API_KEY
    +'&page='

export const getNews =  async (page) => {
    return await axios.get(url+page, {
        validateStatus: function (status) {
            return status === 200;
        }
    })
        .then(response =>{
            return response.data
        })
        .catch(function (error) {
            console.log(error);
        });
}